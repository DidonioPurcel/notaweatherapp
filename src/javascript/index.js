var PAGES_MODULE = require('./pages/pages-module');

var APP_MODULE = 'app';

angular.module(APP_MODULE, [PAGES_MODULE])
    .constant('APP_ID', '86df049b8cfa9df8c174e83ddebb40d7')
    .constant('CITIES', [
        {name: 'London', countryCode: 'uk'},
        {name: 'Amsterdam', countryCode: 'np'},
        {name: 'Kyiv', countryCode: 'ua'},
        {name: 'Budapest', countryCode: 'hu'},
        {name: 'Paris', countryCode: 'fr'}
    ])
    .constant('TIME_FORMAT', 'h:mm a')
    .constant('DATE_FORMAT', 'MMM d');