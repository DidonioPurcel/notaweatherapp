var template = require('./sun-list-template.html');
require('./sun.css');

var NG_NAME = 'sunList';
module.exports = {
    NG_NAME: NG_NAME,
    bindings: {
        citiesSunList: '<' + NG_NAME
    },
    controllerAs: 'sunListCtrl',
    template: template
};