module.exports = SunInfoService;

SunInfoService.NG_NAME = 'sunInfoService';
function SunInfoService($q, weatherApiService) {
    'ngInject';

    return {
        getSunInfo,
        getBulkSunInfo
    };

    function getBulkSunInfo(cities) {
        return $q.all( //of course it would be much better option to make bulk request for sun info
            cities.map(function (city) {
                return getSunInfo(city.name, city.countryCode)
                    .then(function (sunInfo) {
                        return angular.extend({sunInfo: sunInfo}, city)
                    });
            })
        );
    }

    function getSunInfo(cityName, cityCountryCode) {
        return weatherApiService.getCityInfo(cityName, cityCountryCode)
            .then(function (data) {
                var sys = data.sys;
                return {
                    sunRise: sys.sunrise * 1000,
                    sunSet: sys.sunset * 1000
                };
            });
    }
};