var NG_NAME = 'sun';
var template = require('./sun-template.html');

module.exports = {
    NG_NAME: NG_NAME,
    bindings: {
        sunInfo: '<' + NG_NAME + 'Info'
    },
    controller: SunController,
    controllerAs: 'sunCtrl',
    template: template
};

function SunController(TIME_FORMAT) {
    'ngInject';
    var $ctrl = this;

    angular.extend($ctrl, {
        TIME_FORMAT: TIME_FORMAT
    });
}