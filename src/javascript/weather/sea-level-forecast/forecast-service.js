module.exports = ForecastService;

ForecastService.NG_NAME = 'forecastService';
function ForecastService(weatherApiService) {
    'ngInject';
    return {
        getSeaLevelForecast
    };

    function getSeaLevelForecast(cityName, countryCode) {
        return weatherApiService.getCityForecast(cityName, countryCode)
            .then(getSeaLevel);
    }

    function getSeaLevel(forecast) {
        return forecast.list
            .filter(filter9oclock)
            .map(getSeaLevelInfo);
    }

    function filter9oclock(item) {
        return item.dt_txt.includes('09:00');
    }

    function getSeaLevelInfo(item) {
        return {
            seaLevel: item.main.sea_level,
            date: item.dt * 1000
        };
    }
}