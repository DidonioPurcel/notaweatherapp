var NG_NAME = 'seaLevelForecastList';
var template = require('./sea-level-forecast-list-template.html');

module.exports = {
    NG_NAME: NG_NAME,
    bindings: {
        seaLevelForecast: '<' + NG_NAME + 'Forecast'
    },
    controller: SeaLevelForecastController,
    controllerAs: 'seaLevelForecastCtrl',
    template: template
};

function SeaLevelForecastController(DATE_FORMAT) {
    'ngInject';
    var $ctrl = this;

    angular.extend($ctrl, {
        DATE_FORMAT: DATE_FORMAT
    });
}