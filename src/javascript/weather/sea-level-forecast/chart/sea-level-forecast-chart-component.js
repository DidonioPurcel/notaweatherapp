var HighCharts = require('highcharts');

var NG_NAME = 'seaLevelForecastChart';
module.exports = {
    NG_NAME: NG_NAME,
    bindings: {
        seaLevelForecast: '<' + NG_NAME + 'Forecast'
    },
    controller: SeaLevelForecastChartController
};

function SeaLevelForecastChartController($element) {
    'ngInject';
    var $ctrl = this;

    angular.extend($ctrl, {
        $onInit,
        $onChanges
    });

    function $onInit() {
        $element.addClass('d-block');
    }

    function $onChanges() {
        if (!$ctrl.seaLevelForecast) {
            return;
        }

        HighCharts.chart($element[0], {
            title: {
                text: null
            },
            chart: {
                type: 'line'
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, '#f7f7f7'],
                            [1, HighCharts.Color('#f7f7f7').setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null,
                    pointInterval: 3600000 * 24
                }
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Pressure (hPa)'
                }
            },
            colors: ['#474747'],
            series: [{
                type: 'area',
                name: 'Pressure',
                data: prepareData($ctrl.seaLevelForecast)
            }],
            legend: {
                enabled: false
            }
        });
    }

    function prepareData(forecast) {
        return forecast.map(function (item) {
            return [item.date, item.seaLevel];
        });
    }
}