var urls = require('./urls');

module.exports = WeatherApiService;

WeatherApiService.NG_NAME = 'weatherApiService';
function WeatherApiService($http, $interpolate, APP_ID) {
    'ngInject';

    return {
        getCityInfo,
        getCityForecast
    };

    function getCityInfo(name, countryCode) {
        return getForCity(urls.CURRENT_WEATHER, name, countryCode);
    }

    function getCityForecast(name, countryCode) {
        return getForCity(urls.FORECAST, name, countryCode);
    }

    function getForCity(url, name, countryCode) {
        //I decided to use interpolation here since it makes urls in urls file clearer,
        // you can easily see what parts url consists of
        var getUrl = $interpolate(url)({
            name: name,
            countryCode: countryCode,
            appid: APP_ID
        });

        return $http.get(getUrl, {cache: true})
            .then(getData);
    }

    function getData(response) {
        return response.data;
    }
};