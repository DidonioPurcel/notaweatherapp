var NG_NAME = 'weather';

module.exports = {
    NG_NAME: NG_NAME,
    template: '<div class="p-3" ui-view=""></div>'
};
