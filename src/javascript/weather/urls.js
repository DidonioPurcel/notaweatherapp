module.exports = {
    CURRENT_WEATHER: 'http://api.openweathermap.org/data/2.5/weather?q={{name}},{{countryCode}}&appid={{appid}}',
    FORECAST: 'http://api.openweathermap.org/data/2.5/forecast?q={{name}},{{countryCode}}&appid={{appid}}'
};