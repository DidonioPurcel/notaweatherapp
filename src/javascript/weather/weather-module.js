var SunComponent = require('./sun/sun-component');
var WeatherComponent = require('./weather-component');
var SeaLevelForecastListComponent = require('./sea-level-forecast/list/sea-level-forecast-list-component');
var SeaLevelForecastChartComponent = require('./sea-level-forecast/chart/sea-level-forecast-chart-component');
var SunListComponent = require('./sun/list/sun-list-component');

var SunInfoService = require('./sun/sun-info-service');
var WeatherApiService = require('./weather-api-service');
var ForecastService = require('./sea-level-forecast/forecast-service');

var LayoutModule = require('./../layout/layout-module');

var MODULE_NAME = 'WEATHER';
module.exports = MODULE_NAME;

angular.module(MODULE_NAME, [
    LayoutModule
])
    .component(SunComponent.NG_NAME, SunComponent)
    .component(WeatherComponent.NG_NAME, WeatherComponent)
    .component(SeaLevelForecastListComponent.NG_NAME, SeaLevelForecastListComponent)
    .component(SeaLevelForecastChartComponent.NG_NAME, SeaLevelForecastChartComponent)
    .component(SunListComponent.NG_NAME, SunListComponent)
    .factory(SunInfoService.NG_NAME, SunInfoService)
    .factory(WeatherApiService.NG_NAME, WeatherApiService)
    .factory(ForecastService.NG_NAME, ForecastService);