var template = require('./page-header-template.html');

var NG_NAME = 'pageHeader';

module.exports = {
    NG_NAME: NG_NAME,
    template: template,
    transclude: {
        title: 'pageHeaderTitle',
        actions: '?pageHeaderActions'
    },
    controller: function ($element) {
        'ngInject';
        $element.addClass('clearfix');
    }
};