var template = require('./card-template.html');

var NG_NAME = 'card';

module.exports = {
    NG_NAME: NG_NAME,
    template: template,
    transclude: {
        left: NG_NAME + 'Left',
        right: NG_NAME + 'Right'
    },
    controller: function ($element) {
        'ngInject';
        $element.addClass('card bg-faded');
    }
};