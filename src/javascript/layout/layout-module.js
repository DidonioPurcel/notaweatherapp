var CardComponent = require('./card/card-component');
var TitleComponent = require('./page-header/page-header-component');

var MODULE_NAME = 'layout';

module.exports = MODULE_NAME;

angular.module(MODULE_NAME, [])
    .component(CardComponent.NG_NAME, CardComponent)
    .component(TitleComponent.NG_NAME, TitleComponent);