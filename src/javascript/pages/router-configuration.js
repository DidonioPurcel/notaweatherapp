module.exports = function ($stateProvider, $urlRouterProvider) {
    'ngInject';

    $stateProvider
        .state('cities', {
            url: '/cities',
            component: 'cities',
            //initially I made resolve functions heavy, injecting services inside and
            //loading cities` current weather info.
            //The idea behind this was to make components dumb. The less component knows about
            // where to get data, the more it is reusable.
            //But with further development it appeared that cities component is more like container
            //component, that is very unlikely to be reusable.
            //So I returned to concept of smart(container)/dumb components and moved logic
            // for data retrieval into cities (and city) component.
            resolve: {
                citiesList: 'CITIES'
            }
        })
        .state('city', {
            url: '/city/:name/:countryCode',
            component: 'city',
            resolve: {
                cityName: function ($stateParams) {
                    return $stateParams.name;
                },
                cityCountryCode: function ($stateParams) {
                    return $stateParams.countryCode;
                }
            }
        });

    $urlRouterProvider.when('/', '/cities');
    $urlRouterProvider.otherwise('/cities');
}
