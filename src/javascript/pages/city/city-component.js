var NG_NAME = 'city';
var template = require('./city-template.html');

module.exports = {
    NG_NAME: NG_NAME,
    bindings: {
        cityName: '<' + NG_NAME + 'Name',
        cityCountryCode: '<' + NG_NAME + 'CountryCode'
    },
    controller: CityController,
    controllerAs: 'cityCtrl',
    template: template
};

function CityController(DATE_FORMAT, forecastService) {
    'ngInject';
    var $ctrl = this;

    angular.extend($ctrl, {
        DATE_FORMAT: DATE_FORMAT,

        seaLevelForecast: null,
        $onInit
    });

    function $onInit() {
        forecastService.getSeaLevelForecast($ctrl.cityName, $ctrl.cityCountryCode)
            .then(onSeaLevelForecast);
    }

    function onSeaLevelForecast(forecast) {
        $ctrl.seaLevelForecast = forecast;
    }
}