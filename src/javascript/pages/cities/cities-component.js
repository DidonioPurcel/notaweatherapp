var NG_NAME = 'cities';
var template = require('./cities-template.html');

module.exports = {
    NG_NAME: NG_NAME,
    bindings: {
        cities: '<' + NG_NAME + 'List'
    },
    controller: CitiesController,
    controllerAs: 'citiesCtrl',
    template: template
};

function CitiesController(sunInfoService) {
    'ngInject';
    var $ctrl = this;

    angular.extend($ctrl, {
        enhancedCities: null,
        $onInit
    });

    function $onInit() {
        sunInfoService.getBulkSunInfo($ctrl.cities)
            .then(onSunInfo);
    }

    function onSunInfo(cities) {
        $ctrl.enhancedCities = cities;
    }
}
