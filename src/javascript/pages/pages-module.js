var CityComponent = require('./city/city-component');
var CitiesComponent = require('./cities/cities-component');
var RouterConfiguration = require('./router-configuration');
var WeatherModule = require('./../weather/weather-module');

var MODULE_NAME = 'PAGES';
module.exports = MODULE_NAME;

angular.module(MODULE_NAME, [
    'ui.router',
    WeatherModule
])
    .component(CitiesComponent.NG_NAME, CitiesComponent)
    .component(CityComponent.NG_NAME, CityComponent)
    .config(RouterConfiguration);