var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var webpack = require('webpack');
var dist = path.resolve(__dirname, 'dist');

module.exports = {
    entry: {
        vendors: [
            'angular',
            'angular-ui-router/release/angular-ui-router.js',
            'highcharts'
        ],
        bundle: './src/javascript/index.js'
    },
    output: {
        filename: '[name].js',
        path: dist
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'index.html',
            chunksSortMode: function (a, b) {      //by default HtmlWebpackPlugin inserts vendors file script after
                var order = ['vendors', 'bundle'];  //bundle file, this functions fixes this problem
                return order.indexOf(a.names[0]) - order.indexOf(b.names[0]);
            }
        }),
        new HtmlWebpackIncludeAssetsPlugin({
            assets: [
                'css/bootstrap.min.css',
                'css/weather-icons.css',
            ], append: true
        }),
        new CopyWebpackPlugin([
            {
                from: 'node_modules/bootstrap/dist/css/bootstrap.min.css',
                to: dist + '/css'
            },
            {
                from: 'lib/weather-icons-master/css/weather-icons.css',
                to: dist + '/css'
            },
            {
                from: 'lib/weather-icons-master/font',
                to: dist + '/font'
            }
        ])
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|angular.js|bower_components)/,
                loader: 'ng-annotate-loader?add=true' //to avoid specifiying angular injections by hand
            },
            {
                test: /[\/\\]angular\.js$/,
                loader: 'exports-loader?angular' //to avoid requiring of angular in every file
            },
            {
                test: /\.css$/,
                use: [
                    {loader: "style-loader"}, //to require css into js, used only once in a project
                    {loader: "css-loader"}
                ]
            },
            {
                test: /\.html$/,
                use: [{
                    loader: 'html-loader', //to require html into js. Actually, at the beginning I wanted to define
                    //template urls of component (it gives more possibilities for components reuse), but this approach did
                    //not work with running application without server (browser did not let to load file lying in another folder)
                    //so I chose option to insert html into js
                    options: {
                        minimize: true
                    }
                }]
            }
        ]
    },
    devServer: {
        contentBase: '/build'
    }
};