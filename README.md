# README #

## Application description ##
Wonderful simplest application displaying list of 5 Europian cities plus sunrise and sunset time for them. 
Click on the city leads to another page where the forecast of the sea level pressure for clicked city is displayed in two ways: list and chart.

## How to run application ##
#### OPTION 1 - open dist/index.html ####
The application is built to dist folder and pushed to git to be run without preparation steps.

#### OPTION 2 - start webpack dev server ####
To do it, follow this steps:  
- install yarn https://yarnpkg.com/lang/en/docs/install/  
- run 'yarn install' command in the root folder  
- run 'npm start' command in the root folder  
- go to 'localhost:8080' and enjoy the application  


## What tool is the application built with? ##
The application is built with Webpack 2.
According to webpack.config.js, this tool does the following:
//js  
- combines vendors chunk (angular, angular-ui-router, highcharts)  
- starting from index.js, collects all javascript project files into bundle.js chunk.  
- outputs built chunks into dist folder  
//html  
- with help of HtmlWebpackPlugin and HtmlWebpackIncludeAssetsPlugin,   
forms index.html file based on index.html from root folder and puts it into dist folder  
//css  
- with help of CopyWebpackPlugin, copies css and font assets into dist  folder (bootstrap.css, weather-icons css and weather-icons fonts)  

Plus, 'webpack dev server' is used to hot reload the application.

## Technical details
Single page application divided into three parts:  
- layout components  
- pages components (containers, loading data)   
- weather components  (dumb components presenting data got from containers through attributes)  
  
Routing of application is implemented with Angular-UI-Router.  
  
Styling is done with Twitter Bootstrap.  

Special font 'Weather-Icons' is added to display sunrise, sunset, barometer.
This font could be taken from npm, but npm version does not contain barometer icon, so I downloaded font package from the master branch and put it into the lib folder.